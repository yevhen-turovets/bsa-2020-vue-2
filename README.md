Инструкция по запуску

**Backend**  
`cd <project_dir>/backend` - перейти в папку с backend  
`cp .env.example .env` - и в .env прописать доступы в переменные базы данных  
В файле .env нужно поправить данные в переменной APP_CODE_PATH_HOST. Вбейте команду в терминале `pwd` и полученный путь поставьте в переменную.
Будет что-то вроде такого:  
`APP_CODE_PATH_HOST=/Users/EugeneTheGodOfDocker/bsa-2020-vue-2/backend`

Выполните команды:  
`composer install`  
`php artisan key:generate`  
`cd public`  
`rm storage`  
`ln -s ../storage/app/public storage`  
`docker-compose exec workspace bash php artisan storage:link`  
`docker-compose up -d --build`  
`docker-compose exec workspace php artisan migrate --seed`     

**Frontend**  
`cd <project_dir>/frontend` - перейти в папку с frontend  
`cp .env.example .env.local` 
Выполните команды:  
`npm install`  
`npm run serve`
