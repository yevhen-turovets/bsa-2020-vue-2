<?php

use \App\Entity\Album;
use \App\Entity\Customer;
use Illuminate\Database\Seeder;

class AlbumTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Album::class, 20)->create();
    }
}
