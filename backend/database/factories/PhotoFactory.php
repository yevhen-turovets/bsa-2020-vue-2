<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use \App\Entity\Photo;
use \App\Entity\Album;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Photo::class, function (Faker $faker) {
    $now = Carbon::now();

    return [
        'title' => $faker->sentence(3),
        'image_url' => $faker->imageUrl(256, 256),
        'album_id' => Album::query()->inRandomOrder()->first()->id,
        'created_at' => $now->toDateTimeString(),
    ];
});
