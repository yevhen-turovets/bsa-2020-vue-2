<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use \App\Entity\Album;
use \App\Entity\Customer;
use Carbon\Carbon;
use Faker\Generator as Faker;

$factory->define(Album::class, function (Faker $faker) {
    $now = Carbon::now();

    return [
        'title' => $faker->sentence(3),
        'preview_image_url' => $faker->imageUrl(256, 256),
        'user_id' => Customer::query()->inRandomOrder()->first()->id,
        'created_at' => $now->toDateTimeString(),
    ];
});
