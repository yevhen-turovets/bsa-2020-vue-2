<?php

namespace App\Http\Controllers;

use App\Entity\Customer;
use App\Http\Presenter\CustomerArrayPresenter;
use App\Http\Request\Customer\AddCustomerHttpRequest;
use App\Http\Request\Customer\UpdateCustomerHttpRequest;

class CustomerController extends Controller
{
    private $presenter;

    public function __construct(CustomerArrayPresenter $presenter)
    {
        $this->presenter = $presenter;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $customer = Customer::orderBy('id', 'desc')->get();

        return response()->json($customer);
    }

    public function getCustomerByNameEmail($searchQuery)
    {
        $customer = Customer::query()
            ->where('name', 'LIKE', "%{$searchQuery}%")
            ->orWhere('email', 'LIKE', "%{$searchQuery}%")
            ->orderBy('id', 'desc')
            ->get();

        return response()->json($customer);
    }


    public function show($id)
    {
        $customer = Customer::findOrFail($id);

        $customer = $this->presenter->present($customer);

        return response()->json($customer);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddCustomerHttpRequest $request)
    {
        $customer = new Customer();

        $customer->name = $request->name;
        $customer->email = $request->email;

        if ($request->file('image')) {
            $customer->avatar = env('APP_URL') . '/storage/' . $request->file('image')
                    ->store('uploads', 'public');
        }

        $customer->save();

        return response()->json($customer);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCustomerHttpRequest $request, $id)
    {
        $customer = Customer::findOrFail($id);

        $customer->name = $request->name;
        $customer->email = $request->email;

        if ($request->file('image')) {
            $customer->avatar = env('APP_URL') . '/storage/' . $request->file('image')
                    ->store('uploads', 'public');
        }

        $customer->save();

        return response()->json($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::findOrFail($id);

        $customer->delete();

        return response()->json($customer);
    }
}
