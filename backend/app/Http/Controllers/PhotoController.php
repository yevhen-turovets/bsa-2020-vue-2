<?php

namespace App\Http\Controllers;

use App\Entity\Photo;
use Illuminate\Http\Request;

class PhotoController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $photo = new Photo();

        $photo->album_id = $request->album_id;

        if ($request->file('image')) {
            $photo->image_url = env('APP_URL') . '/storage/' . $request->file('image')
                    ->store('uploads', 'public');
        }

        $photo->save();

        return response()->json($photo);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $photo = Photo::findOrFail($id);

        $photo->delete();

        return response()->json($photo);
    }
}
