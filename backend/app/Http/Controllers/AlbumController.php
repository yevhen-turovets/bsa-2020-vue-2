<?php

namespace App\Http\Controllers;

use App\Entity\Album;
use App\Http\Presenter\AlbumArrayPresenter;
use Illuminate\Http\Request;

class AlbumController extends Controller
{
    private $presenter;

    public function __construct(AlbumArrayPresenter $presenter)
    {
        $this->presenter = $presenter;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $albums = Album::orderBy('id', 'desc')->get();

        $albums = $this->presenter->presentCollection($albums);

        return response()->json($albums);
    }

    public function getAlbumByUserId($userId)
    {
        $albums = Album::query()
            ->where('user_id', $userId)
            ->orderBy('id', 'desc')
            ->get();

        $albums = $this->presenter->presentCollection($albums);

        return response()->json($albums);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $album = new Album();

        $album->title = $request->title;
        $album->user_id = $request->user_id;

        if ($request->file('image')) {
            $album->preview_image_url = env('APP_URL') . '/storage/' . $request->file('image')
                    ->store('uploads', 'public');
        }

        $album->save();

        return response()->json($album);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $album = Album::findOrFail($id);

        $album = $this->presenter->present($album);

        return response()->json($album);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $album = Album::findOrFail($id);

        $album->title = $request->title;
        $album->user_id = $request->user_id;

        if ($request->file('image')) {
            $album->preview_image_url = env('APP_URL') . '/storage/' . $request->file('image')
                    ->store('uploads', 'public');
        }

        $album->save();

        return response()->json($request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $album = Album::findOrFail($id);

        $album->delete();

        return response()->json($album);
    }
}
