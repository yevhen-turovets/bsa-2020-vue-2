<?php

declare(strict_types=1);

namespace App\Http\Presenter;

use App\Entity\Customer;
use Illuminate\Support\Collection;

final class CustomerArrayPresenter implements CollectionAsArrayPresenter
{
    private $albumArrayPresenter;

    public function __construct(AlbumArrayPresenter $albumArrayPresenter)
    {
        $this->albumArrayPresenter = $albumArrayPresenter;
    }


    public function present(Customer $customer): array
    {
        return [
            'id' => $customer->getId(),
            'name' => $customer->getname(),
            'email' => $customer->getEmail(),
            'avatar' => $customer->getAvatar(),
            'albums' => $this->albumArrayPresenter->presentCollection($customer->albums)
        ];
    }

    public function presentCollection(Collection $collection): array
    {
        return $collection
            ->map(
                function (Customer $customer) {
                    return $this->present($customer);
                }
            )
            ->all();
    }
}
