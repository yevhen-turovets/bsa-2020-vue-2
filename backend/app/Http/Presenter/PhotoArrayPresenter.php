<?php

declare(strict_types=1);

namespace App\Http\Presenter;

use App\Entity\Photo;
use Illuminate\Support\Collection;

final class PhotoArrayPresenter implements CollectionAsArrayPresenter
{
    public function present(Photo $photo): array
    {
        return [
            'id' => $photo->getId(),
            'title' => $photo->getTitle(),
            'image_url' => $photo->getImageUrl(),
        ];
    }

    public function presentCollection(Collection $collection): array
    {
        return $collection
            ->map(
                function (Photo $photo) {
                    return $this->present($photo);
                }
            )
            ->all();
    }
}
