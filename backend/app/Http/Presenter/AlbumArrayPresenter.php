<?php

declare(strict_types=1);

namespace App\Http\Presenter;

use App\Entity\Album;
use Illuminate\Support\Collection;

final class AlbumArrayPresenter implements CollectionAsArrayPresenter
{
    private $photoArrayPresenter;

    public function __construct(PhotoArrayPresenter $photoArrayPresenter)
    {
        $this->photoArrayPresenter = $photoArrayPresenter;
    }


    public function present(Album $album): array
    {
        return [
            'id' => $album->getId(),
            'title' => $album->getTitle(),
            'preview_image_url' => $album->getPreviewImageUrl(),
            'author_id' => $album->getUserId(),
            'author' => $album->author->getName(),
            'photos' => $this->photoArrayPresenter->presentCollection($album->photos)
        ];
    }

    public function presentCollection(Collection $collection): array
    {
        return $collection
            ->map(
                function (Album $album) {
                    return $this->present($album);
                }
            )
            ->all();
    }
}
