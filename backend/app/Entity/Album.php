<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Album extends Model
{
    protected $table = 'albums';

    protected $fillable = [
        'title',
        'preview_image_url',
        'user_id',
    ];

    public function author(): BelongsTo
    {
        return $this->belongsTo(Customer::class, 'user_id');
    }

    public function photos(): HasMany
    {
        return $this->hasMany(Photo::class);
    }

    public function getId() {
        return $this->id;
    }

    public function getTitle() {
        return $this->title;
    }

    public function getUserId() {
        return $this->user_id;
    }

    public function getPreviewImageUrl() {
        return $this->preview_image_url;
    }
}
