import Vue from 'vue';
import Router from 'vue-router';

const Welcome = () => import('./views/Welcome.vue');

const UsersFeed = () => import('./views/UsersFeed.vue');
const User = () => import('./views/User.vue');
const UserAdd = () => import('./views/UserAdd.vue');
const UserEditDelete = () => import('./views/UserEditDelete.vue');

const AlbumsFeed = () => import('./views/AlbumsFeed.vue');
const Album = () => import('./views/Album.vue');
const AlbumAdd = () => import('./views/AlbumAdd.vue');
const AlbumEditDelete = () => import('./views/AlbumEditDelete.vue');

Vue.use(Router);

const router = new Router({
    base: process.env.BASE_URL,
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'main',
            component: Welcome,
        },
        {
            path: '/users',
            name: 'users-list',
            component: UsersFeed,
        },
        {
            path: '/users/add',
            name: 'user-add',
            component: UserAdd,
        },
        {
            path: '/users/:id',
            name: 'user-detail',
            component: User,
        },
        {
            path: '/users/:id/edit',
            name: 'user-edit-delete',
            component: UserEditDelete,
        },
        {
            path: '/albums',
            name: 'albums-list',
            component: AlbumsFeed,
        },
        {
            path: '/albums/:id',
            name: 'album-detail',
            component: Album,
        },
        {
            path: '/albums/add',
            name: 'album-add',
            component: AlbumAdd,
        },
        {
            path: '/albums/:id/edit',
            name: 'album-edit-delete',
            component: AlbumEditDelete,
        },
    ],
    scrollBehavior: () => ({ x: 0, y: 0 }),
})

export default router;
