import {
    SET_ALBUMS,
    SET_ALBUM,
    DELETE_ALBUM,
    DELETE_PHOTO,
    SET_PHOTO,
} from './mutationTypes';

export default {
    [SET_ALBUMS]: (state, albums) => {
        state.albums = albums;
    },

    [SET_ALBUM]: (state, album) => {
        state.albums = {
            ...state.albums,
            [album.id]: album
        };
    },

    [DELETE_ALBUM]: (state, id) => {
        delete state.albums[id];
    },

    [DELETE_PHOTO]: (state, { albumId, photoIndex }) => {
        state.albums[albumId].photos.splice(photoIndex, 1)
    },

    [SET_PHOTO]: (state, photo) => {
        state.albums[photo.album_id].photos = {
            ...state.albums[photo.album_id].photos,
            [photo.id]: photo
        };
    },
}
