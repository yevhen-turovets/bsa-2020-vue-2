export default {
    getAlbums: state => state.albums,

    getAlbumById: state => id => state.albums[id],
}
