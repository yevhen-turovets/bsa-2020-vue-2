export const SET_ALBUMS = 'setAlbums';
export const SET_ALBUM = 'setAlbum';
export const DELETE_ALBUM = 'deleteAlbum';
export const DELETE_PHOTO = 'deletePhoto';
export const SET_PHOTO = 'setPhoto';
