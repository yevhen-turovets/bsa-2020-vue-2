import {
    SET_ALBUMS,
    SET_ALBUM,
    DELETE_ALBUM,
    DELETE_PHOTO,
    SET_PHOTO,
} from './mutationTypes';
import { SET_LOADING } from '../../mutationTypes';
import axios from 'axios';

export default {
    async getAllAlbums({commit}) {
        commit(SET_LOADING, true, {root: true});

        try {
            return axios.get(process.env.VUE_APP_API_URL + 'albums').then(data => {
                let parseData = JSON.parse(JSON.stringify(data.data));

                commit(SET_ALBUMS, parseData);
                commit(SET_LOADING, false, {root: true});

                return parseData;
            })
        } catch (error) {
            console.log(error);

            commit(SET_LOADING, false, {root: true});
        }
    },

    async searchAlbums({commit}, userId) {
        commit(SET_LOADING, true, {root: true});

        try {
            return axios.get(process.env.VUE_APP_API_URL + 'albums/search/' + userId).then(data => {
                let parseData = JSON.parse(JSON.stringify(data.data));

                commit(SET_ALBUMS, parseData);
                commit(SET_LOADING, false, {root: true});

                return parseData;
            })
        } catch (error) {
            console.log(error);

            commit(SET_LOADING, false, {root: true});
        }
    },

    async fetchAlbumsById({commit}, id) {
        commit(SET_LOADING, true, {root: true});

        try {
            return axios.get(process.env.VUE_APP_API_URL + 'albums/' + id).then(data => {
                let parseData = JSON.parse(JSON.stringify(data.data));

                commit(SET_ALBUM, parseData);
                commit(SET_LOADING, false, {root: true});

                return parseData;
            })
        } catch (error) {
            console.log(error);

            commit(SET_LOADING, false, {root: true});
        }
    },

    async createNewAlbum({commit}, { title, user_id, preview_image_url }) {
        commit(SET_LOADING, true, {root: true});

        try {
            const formData = new FormData();
            formData.append('title', title);
            formData.append('user_id', user_id);

            if(preview_image_url) {
                formData.append("image", preview_image_url);
            }

            return axios.post(process.env.VUE_APP_API_URL + 'albums', formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }).then(data => {
                commit(SET_ALBUM, data.data);
                commit(SET_LOADING, false, {root: true});

                return data.data;
            })
        } catch (error) {
            console.log(error);

            commit(SET_LOADING, false, {root: true});
        }
    },

    async addAlbumPhotos({commit}, { albumId, photo }) {
        commit(SET_LOADING, true, {root: true});

        try {
            const formData = new FormData();
            formData.append('album_id', albumId);

            if(photo) {
                formData.append("image", photo);
            }

            return axios.post(process.env.VUE_APP_API_URL + 'photos', formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }).then(data => {
                commit(SET_PHOTO, data.data);
                commit(SET_LOADING, false, {root: true});

                return data.data;
            })
        } catch (error) {
            console.log(error);

            commit(SET_LOADING, false, {root: true});
        }
    },

    async updateAlbumBuId({commit}, { id, title, user_id, preview_image_url }) {
        commit(SET_LOADING, true, {root: true});

        try {
            const formData = new FormData();
            formData.append('title', title);
            formData.append('user_id', user_id);

            if(preview_image_url) {
                formData.append("image", preview_image_url);
            }

            formData.append('_method', 'PATCH');

            return axios.post(process.env.VUE_APP_API_URL + 'albums/' + id, formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }).then(data => {
                commit(SET_ALBUM, data.data);
                commit(SET_LOADING, false, {root: true});

                return data.data;
            })
        } catch (error) {
            console.log(error);

            commit(SET_LOADING, false, {root: true});
        }
    },

    async deleteAlbum({commit}, id) {
        commit(SET_LOADING, true, {root: true});

        try {
            return axios.delete(process.env.VUE_APP_API_URL + 'albums/' + id).then(data => {
                commit(DELETE_ALBUM, id);
                commit(SET_LOADING, false, {root: true});

                return data.data;
            })
        } catch (error) {
            console.log(error);

            commit(SET_LOADING, false, {root: true});
        }
    },

    async deletePhoto({commit}, {
        albumId: albumId,
        photoId: photoId,
        photoIndex: photoIndex,
    }) {
        commit(SET_LOADING, true, {root: true});

        try {
            return axios.delete(process.env.VUE_APP_API_URL + 'photos/' + photoId).then(data => {
                commit(DELETE_PHOTO, { albumId, photoIndex });
                commit(SET_LOADING, false, {root: true});

                return data.data;
            })
        } catch (error) {
            console.log(error);

            commit(SET_LOADING, false, {root: true});
        }
    },
}
