export const SET_USERS = 'setUsers';
export const SET_USER = 'setUser';
export const DELETE_USER = 'deleteUser';
