import {
    SET_USERS,
    SET_USER,
    DELETE_USER,
} from './mutationTypes';

export default {
    [SET_USERS]: (state, users) => {
        state.users = users;
    },

    [SET_USER]: (state, user) => {
        state.users = {
            ...state.users,
            [user.id]: user
        };
    },

    [DELETE_USER]: (state, id) => {
        delete state.users[id];
    },
}
