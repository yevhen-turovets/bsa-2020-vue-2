export default {
    getUsers: state => state.users,

    getUsersById: state => id => state.users[id],
}
