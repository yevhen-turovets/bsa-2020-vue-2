import { SET_USERS, SET_USER, DELETE_USER } from './mutationTypes';
import { SET_LOADING } from '../../mutationTypes';
import axios from 'axios';

export default {
    async getAllUsers({commit}) {
        commit(SET_LOADING, true, {root: true});

        try {
            return axios.get(process.env.VUE_APP_API_URL + 'users').then(data => {
                commit(SET_USERS, data.data);
                commit(SET_LOADING, false, {root: true});

                return data.data;
            })
        } catch (error) {
            console.log(error);

            commit(SET_LOADING, false, {root: true});
        }
    },

    async searchUsers({commit}, searchQuery) {
        commit(SET_LOADING, true, {root: true});

        try {
            return axios.get(process.env.VUE_APP_API_URL + 'users/search/' + searchQuery).then(data => {
                commit(SET_USERS, data.data);
                commit(SET_LOADING, false, {root: true});

                return data.data;
            })
        } catch (error) {
            console.log(error);

            commit(SET_LOADING, false, {root: true});
        }
    },

    async fetchUserById({commit}, id) {
        commit(SET_LOADING, true, {root: true});

        try {
            return axios.get(process.env.VUE_APP_API_URL + 'users/' + id).then(data => {
                let parseData = JSON.parse(JSON.stringify(data.data));

                commit(SET_USER, parseData);
                commit(SET_LOADING, false, {root: true});

                return parseData;
            })
        } catch (error) {
            console.log(error);

            commit(SET_LOADING, false, {root: true});
        }
    },

    async createNewUser({commit}, { name, email, image }) {
        commit(SET_LOADING, true, {root: true});

        try {
            const formData = new FormData();
            formData.append('name', name);
            formData.append('email', email);

            if(image) {
                formData.append("image", image);
            }

            return axios.post(process.env.VUE_APP_API_URL + 'users', formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }).then(data => {
                commit(SET_USER, data.data);
                commit(SET_LOADING, false, {root: true});

                return data.data;
            })
        } catch (error) {
            console.log(error);

            commit(SET_LOADING, false, {root: true});
        }
    },

    async updateUserBuId({commit}, { id, name, email, image }) {
        commit(SET_LOADING, true, {root: true});

        try {
            const formData = new FormData();
            formData.append('name', name);
            formData.append('email', email);

            if(image) {
                formData.append("image", image);
            }

            formData.append('_method', 'PATCH');

            return axios.post(process.env.VUE_APP_API_URL + 'users/' + id, formData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }).then(data => {
                commit(SET_USER, data.data);
                commit(SET_LOADING, false, {root: true});

                return data.data;
            })
        } catch (error) {
            console.log(error);

            commit(SET_LOADING, false, {root: true});
        }
    },

    async deleteUser({commit}, id) {
        commit(SET_LOADING, true, {root: true});

        try {
            return axios.delete(process.env.VUE_APP_API_URL + 'users/' + id).then(data => {
                commit(DELETE_USER, id);
                commit(SET_LOADING, false, {root: true});

                return data.data;
            })
        } catch (error) {
            console.log(error);

            commit(SET_LOADING, false, {root: true});
        }
    },
}
